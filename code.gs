/**
 * Lists all the users in a domain sorted by first name.
 */
function listAllUsers() {
    var pageToken;
    var page;
    do {
        page = AdminDirectory.Users.list({
            domain: 'INSERT DOMAIN', //Insert domain for G Suite
            orderBy: 'givenName',
            maxResults: 100,
            pageToken: pageToken,
        });
        var users = page.users;
        if (users) {
            for (var i = 0; i < users.length; i++) {
                var user = users[i];

                Logger.log('%s (%s) %s', user.name.fullName, user.primaryEmail, user.phones);
            }
        } else {
            Logger.log('No users found.');
        }
        pageToken = page.nextPageToken;
    } while (pageToken);
}

function getSingleSig() {
    return HtmlService.createTemplateFromFile('sigTemp').evaluate().getContent();
}

function doGet(user) {
    var t = HtmlService.createTemplateFromFile('allSignatureTemp');
    t.data = user;
    //var testing = user.organizations[0].title;

    return t.evaluate().getContent();
}

// Adapted from script at https://stackoverflow.com/questions/40936257/how-to-use-the-gmail-api-oauth2-for-apps-script-and-domain-wide-delegation-to
function getUserData() {
    var userEmail = 'USER@EMAIL.COM'; // Used to update user email 1 at a time.
    return AdminDirectory.Users.get(userEmail);
}

function setSignatureSingleUser() {

    var user = getUserData();
    var email = user.primaryEmail;

    Logger.log('User data:\n %s', user);
    var signature = getSingleSig();

    var test = setSignature(email, signature);

}

function setSignatureAll() {

    var pageToken;
    var page;
    do {
        page = AdminDirectory.Users.list({
            domain: 'INSERT DOMAIN', //Insert domain for G Suite
            orderBy: 'givenName',
            maxResults: 1,
            pageToken: pageToken,
        });
        var users = page.users;
        if (users) {
            for (var i = 0; i < users.length; i++) {
                var user = users[i];
                var email = user.primaryEmail;

                var signature = doGet(user);
                var setSig = setSignature(email, signature);

                Logger.log('test result: ' + user);
            }
        } else {
            Logger.log('No users found.');
        }
        pageToken = page.nextPageToken;
    } while (pageToken);

}


function setSignature(email, signature) {

    Logger.log('starting setSignature');

    var signatureSetSuccessfully = false;

    var service = getDomainWideDelegationService('Gmail: ', 'https://www.googleapis.com/auth/gmail.settings.basic', email);

    if (!service.hasAccess()) {
        Logger.log('failed to authenticate as user ' + email);
        Logger.log(service.getLastError());
        signatureSetSuccessfully = service.getLastError();
        return signatureSetSuccessfully;

    } else Logger.log('successfully authenticated as user ' + email);
    var username = email.split("@")[0];
    var resource = { signature: signature };

    var requestBody                = {};
    requestBody.headers            = {'Authorization': 'Bearer ' + service.getAccessToken()};
    requestBody.contentType        = "application/json";
    requestBody.method             = "PUT";
    requestBody.payload            = JSON.stringify(resource);
    requestBody.muteHttpExceptions = false;

    var emailForUrl = encodeURIComponent(email);
    var url = 'https://www.googleapis.com/gmail/v1/users/me/settings/sendAs/' + emailForUrl;
    var maxSetSignatureAttempts     = 2;
    var currentSetSignatureAttempts = 0;

    do {
        try {
            currentSetSignatureAttempts++;
            Logger.log('currentSetSignatureAttempts: ' + currentSetSignatureAttempts);
            var setSignatureResponse = UrlFetchApp.fetch(url, requestBody);
            Logger.log('setSignatureResponse on successful attempt:' + setSignatureResponse);
            signatureSetSuccessfully = true;
            break;
        } catch(e) {
            Logger.log('set signature failed attempt, waiting 3 seconds and re-trying');
            Utilities.sleep(3000);
        }
        if (currentSetSignatureAttempts >= maxSetSignatureAttempts) {
            Logger.log('exceeded ' + maxSetSignatureAttempts + ' set signature attempts, deleting user and ending script');
            throw new Error('Something went wrong when setting their email signature.');
        }
    } while (!signatureSetSuccessfully);
    return signatureSetSuccessfully;

}

// these two things are included in the .JSON file that you download when creating the service account and service account key
var OAUTH2_SERVICE_ACCOUNT_PRIVATE_KEY  = 'ADD PRIVATE KEY';
var OAUTH2_SERVICE_ACCOUNT_CLIENT_EMAIL = 'SERVICE ACCOUNT ADDRESS';


function getDomainWideDelegationService(serviceName, scope, email) {

    Logger.log('starting getDomainWideDelegationService for email: ' + email);

    return OAuth2.createService(serviceName + email)
    // Set the endpoint URL.
        .setTokenUrl('https://accounts.google.com/o/oauth2/token')

        // Set the private key and issuer.
        .setPrivateKey(OAUTH2_SERVICE_ACCOUNT_PRIVATE_KEY)
        .setIssuer(OAUTH2_SERVICE_ACCOUNT_CLIENT_EMAIL)

        // Set the name of the user to impersonate. This will only work for
        // Google Apps for Work/EDU accounts whose admin has setup domain-wide
        // delegation:
        // https://developers.google.com/identity/protocols/OAuth2ServiceAccount#delegatingauthority
        .setSubject(email)

        // Set the property store where authorized tokens should be persisted.
        .setPropertyStore(PropertiesService.getScriptProperties())

        // Set the scope. This must match one of the scopes configured during the
        // setup of domain-wide delegation.
        .setScope(scope);

}